<?php
require('fpdf183/fpdf.php');
error_reporting(E_ALL);
		ini_set('display_errors', 'On');
		 
		include "DBconnection.php";
		 
		$wiersz = $_POST["wiersz"];
		
		$query = "
		BEGIN
		faktura('$wiersz');
		END;";
		 
		
		 
		$c = oci_connect($username, $password, $database, null, OCI_SYSDBA);
		if (!$c) {
			$m = oci_error();
			trigger_error('Could not connect to database: '. $m['message'], E_USER_ERROR);
		}
		 
		$s = oci_parse($c, $query);
		if (!$s) {
			$m = oci_error($c);
			trigger_error('Could not parse statement: '. $m['message'], E_USER_ERROR);
		}
		$r = oci_execute($s);
		if (!$r) {
			$m = oci_error($s);
			trigger_error('Could not execute statement: '. $m['message'], E_USER_ERROR);
		}
		
		$invoice = oci_fetch_array($s, OCI_ASSOC+OCI_RETURN_NULLS);
	

//A4 width : 219mm
//default margin : 10mm each side
//writable horizontal : 219-(10*2)=189mm

$pdf = new FPDF('P','mm','A4');

$pdf->AddPage();

//set font to arial, bold, 14pt
$pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )

$pdf->Cell(130	,5,'Fabryka Mebli',0,0);
$pdf->Cell(59	,5,'FAKTURA',0,1);//end of line

//set font to arial, regular, 12pt
$pdf->SetFont('Arial','',12);

$pdf->Cell(130	,5,'Twarogowa 3',0,0);
$pdf->Cell(59	,5,'',0,1);//end of line

$pdf->Cell(130	,5,'Rzeszow, Polska, 35-232',0,0);
$pdf->Cell(25	,5,'Data',0,0);
$pdf->Cell(34	,5,($invoice!==null?htmlspecialchars($invoice['DATA_ZAMOWIENIA'], ENT_QUOTES|ENT_SUBSTITUTE):"&nbsp;"),0,1);//end of line

$pdf->Cell(130	,5,'Telefon: +12345678',0,0);
$pdf->Cell(25	,5,'Faktura #',0,0);
$pdf->Cell(34	,5,($invoice!==null?htmlspecialchars($invoice[':B1'], ENT_QUOTES|ENT_SUBSTITUTE):"&nbsp;"),0,1);//end of line

$pdf->Cell(130	,5,'Fax +12345678',0,0);
$pdf->Cell(25	,5,'Klient ID',0,0);
$pdf->Cell(34	,5,$invoice['ID_ODBIORCY'],0,1);//end of line

//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line

//billing address
$pdf->Cell(100	,5,'Rachunek dla:',0,1);//end of line

//add dummy cell at beginning of each line for indentation
$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,$invoice['ID_ODBIORCY'],0,1);

$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,$invoice['ADRES'],0,1);

$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,$invoice['NR_TELEFONU'],0,1);

//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line

//invoice contents
$pdf->SetFont('Arial','B',12);

$pdf->Cell(130	,5,'Opis',1,0);
$pdf->Cell(25	,5,'Liczba',1,0);
$pdf->Cell(34	,5,'Kwota',1,1);//end of line

$pdf->SetFont('Arial','',12);

//Numbers are right-aligned so we give 'R' after new line parameter

//items
$liczba=0;
$amount=0;
$pdf->Cell(130	,5,$invoice['ID_PRODUKTU'],1,0);
$pdf->Cell(25	,5,number_format($invoice['LICZBA']),1,0);
$pdf->Cell(34	,5,number_format($invoice['CENA']),1,1,'R');//end of line
$liczba += $invoice['LICZBA'];
$amount += $invoice['CENA'];
	
$cena = $amount * $liczba;
//summary

$pdf->Cell(130	,5,'',0,0);
$pdf->Cell(25	,5,'Do zaplaty',0,0);
$pdf->Cell(10	,5,'PLN',1,0);
$pdf->Cell(24	,5,number_format($cena),1,1,'R');//end of line
$pdf->Output();
?>
