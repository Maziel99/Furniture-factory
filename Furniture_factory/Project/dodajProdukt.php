<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Fabryka Mebli</title>
</head>
<body>
	<header>
		<h1>Dodaj Produkt</h1>
	</header>
	
	<aside>
		<ul>
			<li><a href="index.php">Admin</a></li>
			<li><a href="zamowienia.php">Zamówienia</a></li>
			<li><a href="materialy.php">Materiały</a></li>
			<li><a href="produkty.php">Produkty</a></li>
			<li><a href="odbiorcy.php">Odbiorcy</a></li>
			<li><a href="dostawcy.php">Dostawcy</a></li>
			<li><a href="pracownicy.php">Pracownicy</a></li>
		</ul>
	</aside>
	
	<main>
		<form method="post" action="dodanieProduktu.php">
		<label>Nazwa:</label><br>
		<input type="text" id="nazwa" name="nazwa"><br>
		<label>Opis:</label><br>
		<input type="text" id="opis" name="opis"><br>
		<label>Cena:</label><br>
		<input type="text" id="cena" name="cena"><br>
		<label>Materiał:</label><br>
		<input type="text" id="material" name="material"><br>
		<input type="submit" value="Dodaj">
		</form> 
	</main>
	
</body>
</html>