<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Fabryka Mebli</title>
</head>
<body>
	<header>
		<h1>Dodaj Pracownika</h1>
	</header>
	
	<aside>
		<ul>
			<li><a href="index.php">Admin</a></li>
			<li><a href="zamowienia.php">Zamówienia</a></li>
			<li><a href="materialy.php">Materiały</a></li>
			<li><a href="produkty.php">Produkty</a></li>
			<li><a href="odbiorcy.php">Odbiorcy</a></li>
			<li><a href="dostawcy.php">Dostawcy</a></li>
			<li><a href="pracownicy.php">Pracownicy</a></li>
		</ul>
	</aside>
	
	<main>
		<form method="post" action="dodaniePracownika.php">
		<label>Imię:</label><br>
		<input type="text" id="imie" name="imie"><br>
		<label>Nazwisko:</label><br>
		<input type="text" id="nazwisko" name="nazwisko"><br>
		<label>Data urodzenia:</label><br>
		<input type="text" id="data_ur" name="data_ur"><br>
		<label>Wynagrodzenie:</label><br>
		<input type="text" id="wynagrodzenie" name="wynagrodzenie"><br>
		<label>Nr_telefonu:</label><br>
		<input type="text" id="nr_telefonu" name="nr_telefonu"><br>
		<label>E-mail:</label><br>
		<input type="text" id="email" name="email"><br>
		<label>Login:</label><br>
		<input type="text" id="login" name="login"><br>
		<label>Hasło:</label><br>
		<input type="text" id="haslo" name="haslo"><br>
		<input type="submit" value="Dodaj">
		</form> 
	</main>
	
</body>
</html>