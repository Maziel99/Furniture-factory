<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Fabryka Mebli</title>
</head>
<body>
	<header>
		<h1>Panel Administratora</h1>
	</header>
	
	<aside>
		<ul>
			<li><a href="index.php">Admin</a></li>
			<li><a href="zamowienia.php">Zamówienia</a></li>
			<li><a href="materialy.php">Materiały</a></li>
			<li><a href="produkty.php">Produkty</a></li>
			<li><a href="odbiorcy.php">Odbiorcy</a></li>
			<li><a href="dostawcy.php">Dostawcy</a></li>
			<li><a href="pracownicy.php">Pracownicy</a></li>
		</ul>
	</aside>
	
	<main>
		<table>
		<tr>
			<td><button id="dodaj" onclick="location.href='dodajDostawce.php'" type="button">Dodaj dostawcę</button></td>
			<td><button id="dodaj" onclick="location.href='dodajOdbiorce.php'" type="button">Dodaj odbiorce</button></td>
			<td><button id="dodaj" onclick="location.href='dodajPracownika.php'" type="button">Dodaj pracownika</button></td>
			<td><button id="dodaj" onclick="location.href='dodajZamowienie.php'" type="button">Dodaj zamówienie</button></td>
			<td><button id="dodaj" onclick="location.href='dodajProdukt.php'" type="button">Dodaj produkt</button></td>
			<td><button id="dodaj" onclick="location.href='dodajMaterial.php'" type="button">Dodaj materiał</button></td>
		</tr>
		</table>
		<table>
		<tr>
			<td><button id="statystyka" onclick="location.href='podwyzki.php'" type="button">Daj podwyżkę</button></td>
			<td><button id="statystyka" onclick="location.href='cofnijpodwyzki.php'" type="button">Cofnij podwyżkę</button></td>
			<td><button id="statystyka" onclick="location.href='bonus.php'" type="button">Bonus dla najlepszego</button></td>
		</tr>
		</table>
	</main>
	
	<footer>

	</footer>
</body>
</html>