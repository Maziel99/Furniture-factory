<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
            
    include "DBconnection.php";
	
	$nazwa = $_POST["nazwa"];
	$opis = $_POST["opis"];
	$cena = $_POST["cena"];
	$material = $_POST["material"];
		 
	$query = 	"BEGIN
				:RESULT := wstawprodukt('$nazwa','$opis','$cena','$material');
				END;";
                
    $c = oci_connect($username, $password, $database, null, OCI_SYSDBA);
        if (!$c) {
        $m = oci_error();
        trigger_error('Could not connect to database: '. $m['message'], E_USER_ERROR);
    }
                
    $s = oci_parse($c, $query);
    if (!$s) {
        $m = oci_error($c);
        trigger_error('Could not parse statement: '. $m['message'], E_USER_ERROR);
    }
        
    oci_bind_by_name($s, ':RESULT', $result, 40);
    oci_execute($s);

    if($result){
        header("REFRESH:0.1; dodajProdukt.php");
        echo "<script>alert('Dodano do bazy produkt ".$nazwa."')</script>";
    }
    else {
        header("REFRESH:0.1, dodajProdukt.php");
        echo "<script>alert('Wystąpił błąd')</script>";
    }
?>