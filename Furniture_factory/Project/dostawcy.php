<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Fabryka Mebli</title>

</head>
<body>
	<header>
		<h1>Dostawcy</h1>
	</header>
	
	<aside>
		<ul>
			<li><a href="index.php">Admin</a></li>
			<li><a href="zamowienia.php">Zamówienia</a></li>
			<li><a href="materialy.php">Materiały</a></li>
			<li><a href="produkty.php">Produkty</a></li>
			<li><a href="odbiorcy.php">Odbiorcy</a></li>
			<li><a href="dostawcy.php">Dostawcy</a></li>
			<li><a href="pracownicy.php">Pracownicy</a></li>
		</ul>
	</aside>
	
	<main>
	<?php
		error_reporting(E_ALL);
		ini_set('display_errors', 'On');
		 
		include 'DBconnection.php';
		 
		$query = "
		BEGIN
		wczytajdostawcy;
		END;";
		 
		$c = oci_connect($username, $password, $database, null, OCI_SYSDBA);
		if (!$c) {
			$m = oci_error();
			trigger_error('Could not connect to database: '. $m['message'], E_USER_ERROR);
		}
		 
		$s = oci_parse($c, $query);
		if (!$s) {
			$m = oci_error($c);
			trigger_error('Could not parse statement: '. $m['message'], E_USER_ERROR);
		}
		$r = oci_execute($s);
		if (!$r) {
			$m = oci_error($s);
			trigger_error('Could not execute statement: '. $m['message'], E_USER_ERROR);
		}
		?>
		<table class='tabela' border='1' id='dostawca'>
		<?php
		echo "<tr>\n";
		echo "<th>ID</th> <th>Nazwa</th> <th>Numer telefonu</th> <th>E-mail</th> <th colspan='2'>Akcje</th>";
		echo "</tr>\n";
		 $i = 1;
		 
		while (($row = oci_fetch_array($s, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
			echo "<tr>\n";
			foreach ($row as $item) {
				echo "<td>";
				echo $item!==null?htmlspecialchars($item, ENT_QUOTES|ENT_SUBSTITUTE):"&nbsp;";
				echo "</td>\n";
				
			}
			echo "<td><button id='aktualizuj' type='button' onclick=aktualizujDostawce(".$i.")>Aktualizuj</button></td>\n";
			echo "<td><button id='usun' type='button' onclick=usunDostawce(".$i.")>Usuń</button></td>\n";
			echo "</tr>\n";
			$i++;
		}
		echo "</table>\n";
	?>
	
	<form method="post" action="usunDostawce.php" id="formularz1">
	</form>
	
	<form method="post" action="aktualizujDostawce.php" id="formularz2">
	</form>
	
	<script>
		for(i = 1; i < document.getElementById("dostawca").rows.length; i++)
			{
				document.getElementById("dostawca").rows[i].cells[0].id="wiersz" + i;
				
			}
			
		function aktualizujDostawce(x){
			let wiersz = document.getElementById("wiersz" + x).innerText;
			let formularz = document.getElementById("formularz2");
			let nazwa = prompt("Nazwa: ","");
			let nr_telefonu = prompt("Nr_telefonu: ","");
			let email = prompt("email: ","");
			if(confirm("Czy chcesz aktualizować tego dostawcę?"))
			{
				let input = document.createElement("input");
				let input1 = document.createElement("input");
				let input2 = document.createElement("input");
				let input3 = document.createElement("input");
				let przycisk = document.createElement("button");
				
				input.value = wiersz;
				input.name = "wiersz";
				
				input1.value = nazwa;
				input1.name = "nazwa";
				input2.value = nr_telefonu;
				input2.name = "nr_telefonu";
				input3.value = email;
				input3.name = "email";
				
				przycisk.type = "submit";
				formularz.appendChild(input);
				formularz.appendChild(input1);
				formularz.appendChild(input2);
				formularz.appendChild(input3);
				formularz.appendChild(przycisk);
				formularz.style.display = "none";
				formularz.submit();
			}
		}
	</script>
	
	<script>
		for(i = 1; i < document.getElementById("dostawca").rows.length; i++)
			{
				document.getElementById("dostawca").rows[i].cells[0].id="wiersz" + i;
				
			}
			
		function usunDostawce(x){
			let wiersz = document.getElementById("wiersz" + x).innerText;
			let formularz = document.getElementById("formularz1");
			if(confirm("Czy chcesz usunąć tego dostawcę?"))
			{
				let input = document.createElement("input");
				let przycisk = document.createElement("button");
				input.value = wiersz;
				input.name = "wiersz";
				przycisk.type = "submit";
				formularz.appendChild(input);
				formularz.appendChild(przycisk);
				formularz.style.display = "none";
				formularz.submit();
			}
		}
	</script>
	</main>
	
	
</body>
</html>