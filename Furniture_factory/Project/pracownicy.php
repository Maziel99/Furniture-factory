<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Fabryka Mebli</title>
</head>
<body>
	<header>
		<h1>Pracownicy</h1>
	</header>
	
	<aside>
		<ul>
			<li><a href="index.php">Admin</a></li>
			<li><a href="zamowienia.php">Zamówienia</a></li>
			<li><a href="materialy.php">Materiały</a></li>
			<li><a href="produkty.php">Produkty</a></li>
			<li><a href="odbiorcy.php">Odbiorcy</a></li>
			<li><a href="dostawcy.php">Dostawcy</a></li>
			<li><a href="pracownicy.php">Pracownicy</a></li>
		</ul>
	</aside>
	
	<main>
	<?php
 
		error_reporting(E_ALL);
		ini_set('display_errors', 'On');
		 
		include "DBconnection.php";
		 
		$query = "
		BEGIN
		wczytajpracownicy;
		END;";
		 
		$c = oci_connect($username, $password, $database, null, OCI_SYSDBA);
		if (!$c) {
			$m = oci_error();
			trigger_error('Could not connect to database: '. $m['message'], E_USER_ERROR);
		}
		 
		$s = oci_parse($c, $query);
		if (!$s) {
			$m = oci_error($c);
			trigger_error('Could not parse statement: '. $m['message'], E_USER_ERROR);
		}
		$r = oci_execute($s);
		if (!$r) {
			$m = oci_error($s);
			trigger_error('Could not execute statement: '. $m['message'], E_USER_ERROR);
		}
		 
		echo "<table class='tabela' border='1' id='pracownicy'>\n";
		echo "<tr>\n";
		echo "<th>ID</th> <th>Imię</th> <th>Nazwisko</th> <th>Data urodzenia</th> <th>Wynagrodzenie</th>
		<th>Numer telefonu</th> <th>E-mail</th>	<th>Login</th> <th>Hasło</th> <th colspan='2'>Akcje</th>";
		echo "</tr>\n";
		$i = 1; 
		while (($row = oci_fetch_array($s, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
			echo "<tr>\n";
			foreach ($row as $item) {
				echo "<td>";
				echo $item!==null?htmlspecialchars($item, ENT_QUOTES|ENT_SUBSTITUTE):"&nbsp;";
				echo "</td>\n";
				
			}
			echo "<td><button id='aktualizuj' type='button' onclick=aktualizujPracownika(".$i.")>Aktualizuj</button></td>\n";
			echo "<td><button id='usun' type='button' onclick=usunPracownika(".$i.")>Usuń</button></td>\n";
			echo "</tr>\n";
			$i++;
		}
		echo "</table>\n";
 
	?>
	
	<form method="post" action="usunPracownika.php" id="formularz1">
	</form>
	
	<form method="post" action="aktualizujPracownika.php" id="formularz2">
	</form>
	
	<script>
		for(i = 1; i < document.getElementById("pracownicy").rows.length; i++)
			{
				document.getElementById("pracownicy").rows[i].cells[0].id="wiersz" + i;
				
			}
			
		function aktualizujPracownika(x){
			let wiersz = document.getElementById("wiersz" + x).innerText;
			let formularz = document.getElementById("formularz2");
			let imie = prompt("Imie: ","");
			let nazwisko = prompt("Nazwiskoa: ","");
			let data_ur = prompt("Data urodzenia: ","");
			let wynagrodzenie = prompt("Wynagrodzenie: ","");
			let nr_telefonu = prompt("Numer telefonu: ","");
			let email = prompt("E-mail: ","");
			let login = prompt("Login: ","");
			let haslo = prompt("Haslo: ","");
			if(confirm("Czy chcesz aktualizować tego pracownika?"))
			{
				let input = document.createElement("input");
				let input1 = document.createElement("input");
				let input2 = document.createElement("input");
				let input3 = document.createElement("input");
				let input4 = document.createElement("input");
				let input5 = document.createElement("input");
				let input6 = document.createElement("input");
				let input7 = document.createElement("input");
				let input8 = document.createElement("input");
				let przycisk = document.createElement("button");
				
				input.value = wiersz;
				input.name = "wiersz";
				
				input1.value = imie;
				input1.name = "imie";
				input2.value = nazwisko;
				input2.name = "nazwisko";
				input3.value = data_ur;
				input3.name = "data_ur";
				input4.value = wynagrodzenie;
				input4.name = "wynagrodzenie";
				input5.value = nr_telefonu;
				input5.name = "nr_telefonu";
				input6.value = email;
				input6.name = "email";
				input7.value = login;
				input7.name = "login";
				input8.value = haslo;
				input8.name = "haslo";
				
				przycisk.type = "submit";
				formularz.appendChild(input);
				formularz.appendChild(input1);
				formularz.appendChild(input2);
				formularz.appendChild(input3);
				formularz.appendChild(input4);
				formularz.appendChild(input5);
				formularz.appendChild(input6);
				formularz.appendChild(input7);
				formularz.appendChild(input8);
				formularz.appendChild(przycisk);
				formularz.style.display = "none";
				formularz.submit();
			}
		}
	</script>
	
	<script>
		for(i = 1; i < document.getElementById("pracownicy").rows.length; i++)
			{
				document.getElementById("pracownicy").rows[i].cells[0].id="wiersz" + i;
				
			}
			
		function usunPracownika(x){
			let wiersz = document.getElementById("wiersz" + x).innerText;
			let formularz = document.getElementById("formularz1");
			if(confirm("Czy chcesz usunąć tego pracownika?"))
			{
				let input = document.createElement("input");
				let przycisk = document.createElement("button");
				input.value = wiersz;
				input.name = "wiersz";
				przycisk.type = "submit";
				formularz.appendChild(input);
				formularz.appendChild(przycisk);
				formularz.style.display = "none";
				formularz.submit();
			}
		}
	</script>
	
	</main>

</body>
</html>