<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Fabryka Mebli</title>
</head>
<body>
	<header>
		<h1>Dodaj dostawcę</h1>
	</header>
	
	<aside>
		<ul>
			<li><a href="index.php">Admin</a></li>
			<li><a href="zamowienia.php">Zamówienia</a></li>
			<li><a href="materialy.php">Materiały</a></li>
			<li><a href="produkty.php">Produkty</a></li>
			<li><a href="odbiorcy.php">Odbiorcy</a></li>
			<li><a href="dostawcy.php">Dostawcy</a></li>
			<li><a href="pracownicy.php">Pracownicy</a></li>
		</ul>
	</aside>
	
	<main>
		<form method="post" action="dodanieMaterialu.php">
		<label>Rodzaj drewna:</label><br>
		<input type="text" id="rodzaj_drewna" name="rodzaj_drewna"><br>
		<label>Dostawca:</label><br>
		<input type="text" id="dostawca" name="dostawca"><br>
		<input type="submit" value="Dodaj">
		</form> 
	</main>
	
	<footer>

	</footer>
</body>
</html>