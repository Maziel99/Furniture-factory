<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Fabryka Mebli</title>
</head>
<body>
	<header>
		<h1>Dodaj Zamówienie</h1>
	</header>
	
	<aside>
		<ul>
			<li><a href="index.php">Admin</a></li>
			<li><a href="zamowienia.php">Zamówienia</a></li>
			<li><a href="materialy.php">Materiały</a></li>
			<li><a href="produkty.php">Produkty</a></li>
			<li><a href="odbiorcy.php">Odbiorcy</a></li>
			<li><a href="dostawcy.php">Dostawcy</a></li>
			<li><a href="pracownicy.php">Pracownicy</a></li>
		</ul>
	</aside>
	
	<main>
		<form method="post" action="dodanieZamowienia.php">
		<label>Produkt:</label><br>
		<input type="text" id="produkt" name="produkt"><br>
		<label>Odbiorca:</label><br>
		<input type="text" id="odbiorca" name="odbiorca"><br>
		<label>Pracownik:</label><br>
		<input type="text" id="pracownik" name="pracownik"><br>
		<label>Liczba:</label><br>
		<input type="text" id="liczba" name="liczba"><br>
		<label>Data zamówienia:</label><br>
		<input type="text" id="data" name="data"><br>
		<label>Termin zamówienia:</label><br>
		<input type="text" id="termin" name="termin"><br>
		<input type="submit" value="Dodaj">
		</form> 
	</main>
	
</body>
</html>