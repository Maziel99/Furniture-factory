--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJPRACOWNICY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJPRACOWNICY" 
AS
cur4 SYS_REFCURSOR;
BEGIN
    OPEN cur4 FOR
    SELECT * FROM PRACOWNICY
    ORDER BY id_pracownika;
    DBMS_SQL.RETURN_RESULT(cur4);
END wczytajpracownicy;

/
