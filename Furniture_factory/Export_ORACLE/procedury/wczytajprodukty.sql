--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJPRODUKTY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJPRODUKTY" 
AS
cur5 SYS_REFCURSOR;
BEGIN
    OPEN cur5 FOR
    SELECT produkty.id_produktu,
    produkty.nazwa, 
    produkty.opis,
    produkty.cena,
    materialy.rodzaj_drewna id_materialu FROM PRODUKTY INNER JOIN MATERIALY
    ON materialy.id_materialu = produkty.id_materialu
    ORDER BY id_produktu;
    DBMS_SQL.RETURN_RESULT(cur5);
END wczytajprodukty;

/
