--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure BONUS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."BONUS" 
AS
bonus_cur SYS_REFCURSOR;
BEGIN
DECLARE
    CURSOR bonus_cur IS
       SELECT pracownicy.id_pracownika, pracownicy.wynagrodzenie
        FROM pracownicy
        WHERE id_pracownika= (
            SELECT zamowienia.id_pracownika
            FROM zamowienia
            WHERE zamowienia.id_zamowienia = (
                SELECT zamowienia.id_zamowienia
                FROM zamowienia
                JOIN produkty ON zamowienia.id_produktu = produkty.id_produktu
                GROUP BY zamowienia.id_zamowienia
                ORDER BY SUM(zamowienia.liczba * produkty.cena) DESC
                FETCH FIRST 1 ROWS ONLY
        )
        GROUP BY zamowienia.id_pracownika
        ORDER BY COUNT(zamowienia.id_zamowienia) DESC
        FETCH FIRST 1 ROWS ONLY)
        FOR UPDATE;
        daj_bonus NUMBER;
        BEGIN
            daj_bonus := .15;
            FOR pracownicy_rec IN bonus_cur LOOP
            UPDATE pracownicy
            SET wynagrodzenie = wynagrodzenie + (wynagrodzenie * daj_bonus)
            WHERE CURRENT OF bonus_cur;
        END LOOP;
    END;
END bonus;

/
