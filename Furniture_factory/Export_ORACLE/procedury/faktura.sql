--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure FAKTURA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."FAKTURA" (i IN zamowienia.id_zamowienia%TYPE)
AS
cur9 SYS_REFCURSOR;
BEGIN
    OPEN cur9 FOR
    SELECT i, produkty.nazwa id_produktu, odbiorcy.nazwa id_odbiorcy, pracownicy.nazwisko id_pracownika, odbiorcy.adres, odbiorcy.nr_telefonu, zamowienia.liczba, produkty.cena, zamowienia.data_zamowienia, zamowienia.termin_zamowienia
    FROM ZAMOWIENIA INNER JOIN PRODUKTY ON zamowienia.id_produktu = produkty.id_produktu
    INNER JOIN ODBIORCY ON zamowienia.id_odbiorcy = odbiorcy.id_odbiorcy
    INNER JOIN PRACOWNICY ON zamowienia.id_pracownika = pracownicy.id_pracownika
    WHERE zamowienia.id_zamowienia = i;
    DBMS_SQL.RETURN_RESULT(cur9);
END faktura;

/
