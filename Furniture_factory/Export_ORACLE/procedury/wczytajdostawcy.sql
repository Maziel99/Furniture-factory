--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJDOSTAWCY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJDOSTAWCY" 
AS
cur1 SYS_REFCURSOR;
BEGIN
    OPEN cur1 FOR
    SELECT * FROM DOSTAWCY
    ORDER BY id_dostawcy;
    DBMS_SQL.RETURN_RESULT(cur1);
END wczytajdostawcy;

/
