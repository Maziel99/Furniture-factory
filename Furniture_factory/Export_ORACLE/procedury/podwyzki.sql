--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure PODWYZKI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."PODWYZKI" 
AS
pracownicy_cur SYS_REFCURSOR;
BEGIN
DECLARE
    CURSOR pracownicy_cur IS
        SELECT  pracownicy.id_pracownika,
                pracownicy.wynagrodzenie
        FROM pracownicy
        FOR UPDATE;
        zwieksz_wyn NUMBER;
    BEGIN
        FOR pracownicy_rec IN pracownicy_cur LOOP
            IF pracownicy_rec.wynagrodzenie < 5000 THEN
                zwieksz_wyn := 200;
            ELSE
                zwieksz_wyn := 100;
            END IF;

            UPDATE pracownicy
            SET wynagrodzenie = wynagrodzenie + zwieksz_wyn
            WHERE CURRENT OF pracownicy_cur;
        END LOOP;
    END;
END podwyzki;

/
