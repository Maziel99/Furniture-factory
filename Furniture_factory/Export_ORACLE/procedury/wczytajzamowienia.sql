--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJZAMOWIENIA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJZAMOWIENIA" 
AS
cur6 SYS_REFCURSOR;
BEGIN
    OPEN cur6 FOR
    SELECT zamowienia.id_zamowienia, produkty.nazwa id_produktu, odbiorcy.nazwa id_odbiorcy, pracownicy.nazwisko id_pracownika, zamowienia.liczba, zamowienia.data_zamowienia, zamowienia.termin_zamowienia
    FROM ZAMOWIENIA INNER JOIN PRODUKTY ON zamowienia.id_produktu = produkty.id_produktu
    INNER JOIN ODBIORCY ON zamowienia.id_odbiorcy = odbiorcy.id_odbiorcy
    INNER JOIN PRACOWNICY ON zamowienia.id_pracownika = pracownicy.id_pracownika
    ORDER BY id_zamowienia;
    DBMS_SQL.RETURN_RESULT(cur6);
END wczytajzamowienia;

/
