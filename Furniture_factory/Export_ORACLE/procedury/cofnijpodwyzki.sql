--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure COFNIJPODWYZKI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."COFNIJPODWYZKI" 
AS
pracownicy_cur2 SYS_REFCURSOR;
BEGIN
DECLARE
    CURSOR pracownicy_cur2 IS
        SELECT  pracownicy.id_pracownika,
                pracownicy.wynagrodzenie
        FROM pracownicy
        FOR UPDATE;
        zmniejsz_wyn NUMBER;
    BEGIN
        FOR pracownicy_rec IN pracownicy_cur2 LOOP
            IF pracownicy_rec.wynagrodzenie < 5000 THEN
                zmniejsz_wyn := 200;
            ELSE
                zmniejsz_wyn := 100;
            END IF;

            UPDATE pracownicy
            SET wynagrodzenie = wynagrodzenie - zmniejsz_wyn
            WHERE CURRENT OF pracownicy_cur2;
        END LOOP;
    END;
END cofnijpodwyzki;

/
