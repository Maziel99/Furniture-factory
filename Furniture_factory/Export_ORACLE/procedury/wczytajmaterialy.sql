--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJMATERIALY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJMATERIALY" 
AS
cur2 SYS_REFCURSOR;
BEGIN
    OPEN cur2 FOR
    SELECT materialy.id_materialu,
    materialy.rodzaj_drewna, 
    dostawcy.nazwa id_dostawcy FROM MATERIALY INNER JOIN DOSTAWCY
    ON materialy.id_dostawcy = dostawcy.id_dostawcy
    ORDER BY id_materialu;
    DBMS_SQL.RETURN_RESULT(cur2);
END wczytajmaterialy;

/
