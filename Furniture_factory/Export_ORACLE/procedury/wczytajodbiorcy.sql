--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJODBIORCY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJODBIORCY" 
AS
cur3 SYS_REFCURSOR;
BEGIN
    OPEN cur3 FOR
    SELECT * FROM ODBIORCY
    ORDER BY id_odbiorcy;
    DBMS_SQL.RETURN_RESULT(cur3);
END wczytajodbiorcy;

/
