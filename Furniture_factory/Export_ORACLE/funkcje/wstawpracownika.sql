--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function WSTAWPRACOWNIKA
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."WSTAWPRACOWNIKA" (
    imie IN pracownicy.imie%TYPE,
    nazwisko IN pracownicy.nazwisko%TYPE,
    data_urodzenia IN pracownicy.data_urodzenia%TYPE,
    wynagrodzenie IN pracownicy.wynagrodzenie%TYPE,
    nr_telefonu IN pracownicy.nr_telefonu%TYPE,
    email IN pracownicy.email%TYPE,
    login IN pracownicy.login%TYPE,
    haslo IN pracownicy.haslo%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(300);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_pracownika) INTO i FROM pracownicy;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
    END;
    INSERT INTO pracownicy VALUES(i, imie, nazwisko, data_urodzenia, wynagrodzenie, nr_telefonu, email, login, haslo);
    alert := 'Dodano pracownika';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
