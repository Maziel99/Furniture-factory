--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function WSTAWMATERIAL
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."WSTAWMATERIAL" (
    rodzaj_drewna IN materialy.rodzaj_drewna%TYPE,
    id_dostawcy IN materialy.id_dostawcy%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(50);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_materialu) INTO i FROM materialy;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
    END;
    INSERT INTO materialy VALUES(i, rodzaj_drewna, id_dostawcy);
    alert := 'Dodano material';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
