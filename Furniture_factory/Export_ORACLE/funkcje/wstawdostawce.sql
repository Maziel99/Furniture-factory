--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function WSTAWDOSTAWCE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."WSTAWDOSTAWCE" (
    nazwa IN dostawcy.nazwa%TYPE,
    nr_telefonu IN dostawcy.nr_telefonu%TYPE,
    email IN dostawcy.email%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(20);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_dostawcy) INTO i FROM dostawcy;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
    END;
    INSERT INTO dostawcy VALUES(i, nazwa, nr_telefonu, email);
    alert := 'Dodano dostawce';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;


/
