--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function AKTUALIZUJPRODUKT
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."AKTUALIZUJPRODUKT" (i IN produkty.id_produktu%TYPE,
    e IN produkty.nazwa%TYPE,
    r IN produkty.opis%TYPE,
    t IN produkty.cena%TYPE,
    y IN produkty.id_materialu%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(200);
BEGIN
    BEGIN
        UPDATE produkty
        SET 
            nazwa = e,
            opis = r,
            cena = t,
            id_materialu = y
        WHERE id_produktu=i;
        a := 'updated';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
