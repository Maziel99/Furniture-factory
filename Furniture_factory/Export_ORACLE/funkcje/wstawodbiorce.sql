--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function WSTAWODBIORCE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."WSTAWODBIORCE" (
    nazwa IN odbiorcy.nazwa%TYPE,
    adres IN odbiorcy.adres%TYPE,
    nr_telefonu IN odbiorcy.nr_telefonu%TYPE,
    email IN odbiorcy.email%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(100);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_odbiorcy) INTO i FROM odbiorcy;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
    END;
    INSERT INTO odbiorcy VALUES(i, nazwa, adres,nr_telefonu, email);
    alert := 'Dodano odbiorce';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
