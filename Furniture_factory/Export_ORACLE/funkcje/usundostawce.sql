--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function USUNDOSTAWCE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."USUNDOSTAWCE" (i IN dostawcy.id_dostawcy%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(30);
BEGIN
    BEGIN
        DELETE FROM dostawcy WHERE id_dostawcy=i;
        a := 'deleted';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
