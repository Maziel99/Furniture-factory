--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function AKTUALIZUJDOSTAWCE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."AKTUALIZUJDOSTAWCE" (i IN dostawcy.id_dostawcy%TYPE,
e IN dostawcy.nazwa%TYPE,
    r IN dostawcy.nr_telefonu%TYPE,
    t IN dostawcy.email%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(200);
BEGIN
    BEGIN
        UPDATE dostawcy
        SET 
            nazwa = e,
            nr_telefonu = r,
            email = t
        WHERE id_dostawcy=i;
        a := 'updated';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
