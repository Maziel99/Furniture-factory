--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function AKTUALIZUJPRACOWNIKA
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."AKTUALIZUJPRACOWNIKA" (i IN pracownicy.id_pracownika%TYPE,
    e IN pracownicy.imie%TYPE,
    r IN pracownicy.nazwisko%TYPE,
    t IN pracownicy.data_urodzenia%TYPE,
    y IN pracownicy.wynagrodzenie%TYPE,
    u IN pracownicy.nr_telefonu%TYPE,
    o IN pracownicy.email%TYPE,
    p IN pracownicy.login%TYPE,
    q IN pracownicy.haslo%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(200);
BEGIN
    BEGIN
        UPDATE pracownicy
        SET 
            imie = e,
            nazwisko = r,
            data_urodzenia = t,
            wynagrodzenie = y,
            nr_telefonu = u,
            email = o,
            login = p,
            haslo = q
        WHERE id_pracownika=i;
        a := 'updated';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
