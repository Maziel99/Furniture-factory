--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function AKTUALIZUJMATERIALY
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."AKTUALIZUJMATERIALY" (i IN materialy.id_materialu%TYPE,
e IN materialy.rodzaj_drewna%TYPE,
    r IN materialy.id_dostawcy%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(200);
BEGIN
    BEGIN
        UPDATE materialy
        SET 
            rodzaj_drewna = e,
            id_dostawcy = r
        WHERE id_materialu=i;
        a := 'updated';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
