--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function USUNPRACOWNIKA
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."USUNPRACOWNIKA" (i IN pracownicy.id_pracownika%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(30);
BEGIN
    BEGIN
        DELETE FROM pracownicy WHERE id_pracownika=i;
        a := 'deleted';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
