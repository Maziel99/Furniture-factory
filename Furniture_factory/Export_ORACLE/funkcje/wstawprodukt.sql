--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function WSTAWPRODUKT
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."WSTAWPRODUKT" (
    nazwa IN produkty.nazwa%TYPE,
    opis IN produkty.opis%TYPE,
    cena IN produkty.cena%TYPE,
    material IN produkty.id_materialu%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(50);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_produktu) INTO i FROM produkty;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
    END;
    INSERT INTO produkty VALUES(i, nazwa, opis, cena, material);
    alert := 'Dodano produkt';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
