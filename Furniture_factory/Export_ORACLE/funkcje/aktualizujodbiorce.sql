--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function AKTUALIZUJODBIORCE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."AKTUALIZUJODBIORCE" (i IN odbiorcy.id_odbiorcy%TYPE,
e IN odbiorcy.nazwa%TYPE,
y IN odbiorcy.adres%TYPE,
    r IN odbiorcy.nr_telefonu%TYPE,
    t IN odbiorcy.email%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(200);
BEGIN
    BEGIN
        UPDATE odbiorcy
        SET 
            nazwa = e,
            adres = y,
            nr_telefonu = r,
            email = t
        WHERE id_odbiorcy=i;
        a := 'updated';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
