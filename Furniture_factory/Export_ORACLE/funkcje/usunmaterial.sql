--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function USUNMATERIAL
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."USUNMATERIAL" (i IN materialy.id_materialu%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(30);
BEGIN
    BEGIN
        DELETE FROM materialy WHERE id_materialu = i;
        a := 'deleted';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
