--------------------------------------------------------
--  File created - pi�tek-czerwca-04-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function WSTAWZAMOWIENIE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."WSTAWZAMOWIENIE" (
    produkt IN zamowienia.id_produktu%TYPE,
    odbiorca IN zamowienia.id_odbiorcy%TYPE,
    pracownik IN zamowienia.id_pracownika%TYPE,
    liczba IN zamowienia.liczba%TYPE,
    data_z IN zamowienia.data_zamowienia%TYPE,
    termin IN zamowienia.termin_zamowienia%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(200);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_zamowienia) INTO i FROM zamowienia;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
    END;
    INSERT INTO zamowienia VALUES(i, produkt, odbiorca, pracownik, liczba, data_z, termin);
    alert := 'Dodano produkt';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
